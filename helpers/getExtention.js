const getExtention = (str) => {
  return str.match(/(?<=\.)[^.]+$/)[0].toLowerCase() || "";
}; 

module.exports = getExtention;