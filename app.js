const fs = require("fs");
const path = require("path");
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");

const router = require("./routes/routes");

const dir = path.join(__dirname, "api", "files");
const apiFolder = path.join(__dirname, "api");
const loggerFile = path.join(__dirname, "logs.log");

try {
  if (!fs.existsSync(apiFolder)) {
    fs.mkdirSync(apiFolder);
  }
} catch (error) {
  console.log(error);
}


try {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
} catch (error) {
  console.log(error);
}

try {
  if (!fs.existsSync(loggerFile)) {
    fs.appendFileSync(loggerFile, "", "utf8");
  }
} catch (error) {
  console.log(error);
}

const PORT = 8080;
const app = express();

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "logs.log"),
  { flags: "a" }
);
app.use(morgan("combined", { stream: accessLogStream }));

app.use(express.json());
app.use(
  cors({
    origin: "*",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    allowedHeaders: "Content-Type",
  })
);

app.use("/api/files", router);
app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).json({
    message: "Internal server error",
  });
})

app.listen(PORT, (err) => {
  if (err) {
    console.log(err);
  }
  console.log("server is running at port " + PORT);
});
